import React from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity, FlatList, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')}/>
                </View>
                <View style={styles.titleLogo}>
                    <Text style={{fontSize: 20, color: '#3EC6FF'}}>PORTOFOLIO</Text>
                </View>
                <View style={styles.registerLogo}>
                    <Text style={{fontSize: 20, color: '#003366'}}>Login</Text>
                </View>
                <View style={styles.form}>
                    <Text>Username</Text>
                    <TextInput style={{borderColor: '#003366', borderRadius: 12}}></TextInput>
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: {
        height: 100,
        backgroundColor: 'white',
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleLogo: {
        flexDirection: 'row-reverse',
        paddingHorizontal: 50
    },
    registerLogo: {
        flexDirection: 'row',
        alignSelf: 'center',
        paddingTop: 80
    }
}
)