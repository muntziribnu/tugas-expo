import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Component from './Latihan/Component/Component';
import Lat2 from './Latihan/Latihan 2/index';
import YouTubeUI from './Tugas/Tugas 12/App';
import SanberUI from './Tugas/Tugas 13/LoginScreen';
import Quiz from './Quiz3/index';

export default function App() {
  return (
    <Quiz />
    //<SanberUI />
    // <Lat2 />
    // <YouTubeUI />
    //  <Component />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
